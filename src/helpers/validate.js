//file: validate.js

export default {
    isURL(string) {
        try {
            new URL(string.trim());
            return true;

        } catch (e) {
            return false;
        }

    },
    isEmpty(string) {
        return string.trim().length === 0;
    }
}

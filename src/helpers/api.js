//file: api.js

import axios from 'axios';

const API = axios.create({
    // baseURL: 'http://localhost:3456',
    baseURL: 'https://api.afftesting.com',
    headers: {
        'Authorization': 'adminadmin'
    }
})

export default {
    async affTesting({url, device, country, os_version}) {
        try {
            let requestBody = {url, device, country, os_version};

            // let response = await axios.post('/afftesting', requestBody);
            let response = await API.post('/afftesting', requestBody);
            
            return response.data;
        } catch (e) {

            if (e.response.data) {
                return {error: e.toString()};
            }

            return {error: e.toString()};
        }
    },

    async getAgencyInfo({url}) {
        try {
            let response = await API.get('/agency/info', {
                params: {
                    url
                }
            });

            return response.data;
        } catch (e) {
            return false;
        }
    }
}

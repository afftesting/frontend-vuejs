//file: devices.js

const all = [
    {label: 'Android', value: 'android', platform: 'android'},
    {label: 'iPhone', value: 'iphone', platform: 'ios'},
    {label: 'iPad', value: 'ipad', platform: 'ios'},
    {label: 'Desktop', value: 'desktop', platform: 'desktop'},
    {label: 'Windows Phone', value: 'windowsphone', platform: 'windowsphone'},
    {label: 'Blackberry', value: 'blackberry', platform: 'blackberry'},
];

export default {
    getAll() {
        return [...all];
    },
    getPlatform(deviceValue) {
        for (let device of all) {
            if (device.value === deviceValue) {
                return device.platform
            }
        }

        return ''
    }
}
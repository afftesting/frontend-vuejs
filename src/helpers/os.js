//file: os.js

const all = [
    {platform: 'ios', label: 'iOS 11', value: 'ios11'},
    {platform: 'ios', label: 'iOS 10.1', value: 'ios10.1'},
    {platform: 'ios', label: 'iOS 9.0', value: 'ios9.0'},
    {platform: 'ios', label: 'iOS 8.0', value: 'ios8.0'},
    {platform: 'ios', label: 'iOS 7.0', value: 'ios7.0'},
    {platform: 'android', label: '8 Oreo', value: 'android8.0'},
    {platform: 'android', label: '7.1.1', value: 'android7.1.1'},
    {platform: 'android', label: '6.0', value: 'android6.0'},
    {platform: 'android', label: '5.0', value: 'android5.0'},
    {platform: 'android', label: '4.4', value: 'android4.4'},
    {platform: 'android', label: '4.1', value: 'android4.1'},
    {platform: 'desktop', label: 'Windows 10', value: 'windows10'},
    {platform: 'windowsphone', label: 'Windows Phone 8.1', value: 'wp8.1'},
    {platform: 'blackberry', label: 'BlackBerry 7.1', value: 'bb7.1'},
];


export default {
    getAll() {
        return [...all];
    },
    getFirstOS(platform) {
        for (let os of all) {
            if (os.platform === platform) {
                return os.value
            }
        }

        return ''
    }
}
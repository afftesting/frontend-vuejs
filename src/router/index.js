//file: index.js

import Vue from 'vue';
import Router from 'vue-router'

import Home from '@/pages/Home';
import About from '@/pages/About';
import Login from '@/pages/account/Login';
import Register from '@/pages/account/Register';
import ForgotPassword from '@/pages/account/ForgotPassword';

Vue.use(Router);

const router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/about',
            name: 'about',
            component: About
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/register',
            name: 'register',
            component: Register
        },
        {
            path: '/forgot-password',
            name: 'forgot-password',
            component: ForgotPassword
        },
    ]
});

export default router;
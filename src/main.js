import Vue from 'vue'
import App from './App.vue'
import 'at-ui-style'
import AtUI from 'at-ui'
import VueClipboard from 'vue-clipboard2'

import router from '@/router'

Vue.use(AtUI);
Vue.use(VueClipboard);

new Vue({
    el: '#app',
    router,
    ...App
});